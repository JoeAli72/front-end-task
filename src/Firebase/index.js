import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: 'AIzaSyC5uFj7C9SJJjQFWXsR2V_UOBrPKVd9bL4',
    authDomain: "buseet-b77c8.firebaseapp.com",
    databaseURL: "https://buseet-b77c8-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "buseet-b77c8",
    storageBucket: "buseet-b77c8.appspot.com",
    messagingSenderId: "494179154132",
    appId: "1:494179154132:web:7fb9b6d5e3bf4a21b76341",
    measurementId: "G-VB4XCNPGVN"
};

// Initialize Firebase
firebase.initializeApp(config);
export const db = firebase.firestore();
export const auth = firebase.auth();

export default firebase;