import { combineReducers } from "redux";
import { firebaseReducer } from 'react-redux-firebase';
import { AddTodo } from './addTodoReducer';
import { firestoreReducer } from 'redux-firestore' // <- needed if using firestore


export default combineReducers({
    addtodo: AddTodo,
    firebase: firebaseReducer,
    firestore: firestoreReducer,
});