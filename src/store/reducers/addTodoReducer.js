import * as actions from "../actions/actionTypes";

const initState = {
  loading: false,
  error:  null,
};

export const AddTodo = (state = initState, action) => {
  switch (action.type) {
    case actions.ADD_TODO_START:
      return { ...state, loading: true };
    case actions.ADD_TODO_SUCCESS:
      return { ...state, loading: false,  error: false};
    case actions.ADD_TODO_FAIL:
      return { ...state, loading: false, error: true };
    default:
      return state;
  }
};
