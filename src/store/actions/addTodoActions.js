import * as actions from "./actionTypes";

export const addTodo =
  (enteredTodoTitle, enteredTodoDescription) => async (dispatch, getState, {getFirestore, getFirebase}) => {
    const fireStore = getFirestore();
    const userId = getState().firebase.auth.uid;
    dispatch({ type: actions.ADD_TODO_START });
    try {
      // await db
      //   .collection("todos")
      //   .get()
      //   .then((snapshot) => {
      //     snapshot.docs.forEach((doc) => {
      //       console.log(doc.data());
      //     });
      //   });
      await fireStore
        .collection("todos")
        .doc(userId)
        .update({ todos: fireStore.FieldValue.arrayUnion(enteredTodoTitle, enteredTodoDescription)});
      dispatch({ type: actions.ADD_TODO_SUCCESS });
    } catch (err) {
      dispatch({ type: actions.ADD_TODO_FAIL, payload: err.message });
    }
  };
