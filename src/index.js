import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import "./index.css";
import App from "./App";
import { AuthContextProvider } from "./store/auth-context";

// Redux
import { Provider } from "react-redux";
import store from "./store";

import { ReactReduxFirebaseProvider } from "react-redux-firebase";
import { rrfProps } from './store';

ReactDOM.render(
  <Provider store={store}>
    <ReactReduxFirebaseProvider {...rrfProps} >
      <AuthContextProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </AuthContextProvider>
    </ReactReduxFirebaseProvider>
  </Provider>,
  document.getElementById("root")
);
