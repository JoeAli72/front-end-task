import Styles from './StartingPageContent.module.css';

const StartingPageContent = () => {
  return (
    <section className={Styles.starting}>
      <h1>Welcome on Board!</h1>
    </section>
  );
};

export default StartingPageContent;
