import Classes from "./TodosContent.module.css";

import AddTodo from "./AddTodo";
import TodosItem from './TodosItem'

const TodosContent = (props) => {
  return (
    <section className={Classes.Section}>
      <div className="container">
        <div className="text-center">
          <h3 className={Classes.Section_title}>Todos List</h3>
          <AddTodo />
          <TodosItem />
        </div>
      </div>
    </section>
  );
};

export default TodosContent;
