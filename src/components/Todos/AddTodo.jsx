import { useState, useRef, useEffect, useCallback } from "react";

import { useSelector, useDispatch } from "react-redux";
// import { useFirestore } from "react-redux-firebase";

import { Modal, Button, Form } from "react-bootstrap";

// import * as actions from "../../store/actions";

import { db, auth } from "../../Firebase";
import firebase from "../../Firebase";

export default function AddTodo() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const formTodoTitleRef = useRef();
  const formTodoDescriptionRef = useRef();

  // const dispatch = useDispatch();

  // const firestore = useFirestore();

  const handleSubmit = async (e) => {
    e.preventDefault();
    // const enteredTodoTitle = formTodoTitleRef.current.value;
    // const enteredTodoDescription = formTodoDescriptionRef.current.value;

    // dispatch(actions.addTodo({title:enteredTodoTitle, des:enteredTodoDescription}));

    // const todo = {
    //   title: enteredTodoTitle,
    //   des: enteredTodoDescription,
    // };
    // await firestore.collection("todos").add(todo);
    // handleClose();
  };


  function addNewTodo(e) {
    e.preventDefault();

    // const userId = auth;
    // console.log(userId);

    const enteredTodoTitle = formTodoTitleRef.current.value;
    const enteredTodoDescription = formTodoDescriptionRef.current.value;

    db.collection("todos").add({
      inprogress: true,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      todoTitle: enteredTodoTitle,
      todoDec: enteredTodoDescription,
    });
    handleClose(); 
  }

  

  return (
    <>
      <Button data-testid="test-1" variant="warning" onClick={handleShow}>
        Add New Todo
      </Button>

      <Modal data-testid="test-modal" show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Todo</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={addNewTodo}>
            <Form.Group className="mb-3" controlId="formTodoTitle">
              <Form.Label>Todo Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Type your todo Title here"
                name="formTodoTitle"
                ref={formTodoTitleRef}
                min="3"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formTodoDescription">
              <Form.Label>Todo Description </Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Type your todo Description here"
                rows={3}
                name="formTodoDescription"
                ref={formTodoDescriptionRef}
                min="15"
                required
              />
            </Form.Group>
            <Button  variant="primary" type="submit">
              Add
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}
