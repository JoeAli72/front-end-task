import { useState, useEffect, useCallback, useRef } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Row, Container, Col, Modal, Button, Form } from "react-bootstrap";

import { Link } from "react-router-dom";

// import { useFirestore } from "react-redux-firebase";
// import { useSelector } from "react-redux";
// import { useFirestoreConnect } from "react-redux-firebase";

import { db } from "../../Firebase";

import "./todoItem.css";

export default function TodosItem({ addTodo, id }) {
  const [todos, setTodos] = useState(null);
  const [inprogress, setInprogress] = useState([]);
  const [doneTodo, setDoneTodo] = useState({});

  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);

  const formTodoTitleRef = useRef();
  const formTodoDescriptionRef = useRef();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  const [todosHeadsState, setTodosHeadsState] = useState([
    { id: "1", title: "Todo" },
    { id: "2", title: "Inprogress" },
    { id: "3", title: "Done" },
  ]);

  const [todoData, setTodoData] = useState({
    todoTitle: "",
    todoDec: "",
    id: "",
  });

  const getTodos = useCallback(() => {
    db.collection("todos").onSnapshot(function (querySnapshot) {
      setTodos(
        querySnapshot.docs.map((doc) => ({
          id: doc.id,
          todoTitle: doc.data().todoTitle,
          todoDec: doc.data().todoDec,
          inprogress: doc.data().inprogress,
        }))
      );
    });
  }, []);

  useEffect(() => {
    getTodos();
  }, [getTodos]);

  const onDragEnd = (result) => {
    // const { source, destination } = result;

    console.log(todos);
    console.log(inprogress);
    console.log(result);

    if (!result.destination) return;

    if (
      result.destination.droppableId === result.source.droppableId &&
      result.destination.index === result.source.index
    )
      return;

    let add,
      active = todos,
      inprog = inprogress,
      done = doneTodo;

    console.log(active);

    if (result.source.droppableId === "TodosList") {
      add = active[result.source.index];
      active.splice(result.source.index, 1);
    } else {
      add = inprog[result.source.index];
      inprog.splice(result.source.index, 1);
    }

    if (result.destination.droppableId === "TodosList") {
      active.splice(result.destination.index, 0, add);
    } else {
      inprog.splice(result.destination.index, 0, add);
    }

    setInprogress(inprog);
    setTodos(active);
  };

  let todosList;
  if (todos) {
    todosList = todos.map((todo, index) => (
      <>
        <Droppable droppableId="TodosList">
          {(provided) => (
            <Col
              lg={3}
              key={todo.id}
              ref={provided.innerRef}
              {...provided.droppableProps}
            >
              <Draggable draggableId={todo.id} index={index}>
                {(provided) => (
                  <Link
                    to="#"
                    id={todo.id}
                    className="todosHead_todos"
                    onClick={(e) => {
                      e.preventDefault();
                      setTodoData({
                        todoTitle: todo.todoTitle,
                        todoDec: todo.todoDec,
                        id: todo.id,
                      });
                      handleShow();
                    }}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    innerRef={provided.innerRef}
                  >
                    <h5 className="todosHead_todos_secTitle">
                      {todo.todoTitle}
                    </h5>
                  </Link>
                )}
              </Draggable>

              {provided.placeholder}
            </Col>
          )}
        </Droppable>
        <Droppable droppableId="InprogressList">
          {(provided) => (
            <Col lg={3} ref={provided.innerRef} {...provided.droppableProps}>
              <Link
                to="#"
                id={todo.id}
                className="todosHead_todos"
                onClick={(e) => {
                  e.preventDefault();
                  setTodoData({
                    todoTitle: todo.todoTitle,
                    todoDec: todo.todoDec,
                    id: todo.id,
                  });
                  handleShow();
                }}
              >
                <h5 className="todosHead_todos_secTitle"></h5>
              </Link>
              {provided.placeholder}
            </Col>
          )}
        </Droppable>

        <Col lg={3}>
          <Link to="#" className="todosHead_todos">
            <h5 className="todosHead_todos_secTitle"></h5>
          </Link>
        </Col>
        <Col lg={3}>
          <div className="options h-100 d-flex align-items-center justify-content-center">
            <Button
              variant="success"
              className="me-2"
              onClick={() => {
                setTodoData({
                  todoTitle: todo.todoTitle,
                  todoDec: todo.todoDec,
                  id: todo.id,
                });
                handleShow2();
              }}
            >
              Edite
            </Button>
            <Button
              variant="danger"
              onClick={() => {
                db.collection("todos").doc(todo.id).delete();
              }}
            >
              Delete
            </Button>
          </div>
        </Col>
      </>
    ));
  }

  const handleChange = (e) => {
    const enteredTitleRef = formTodoTitleRef.current.value;
    const enteredDecRef = formTodoDescriptionRef.current.value;

    setTodoData({
      ...todoData,
      todoTitle: enteredTitleRef,
      todoDec: enteredDecRef,
    });
  };

  return (
    <>
      <DragDropContext onDragEnd={onDragEnd}>
        <div className="todosHead">
          <Container>
            <Row className="g-0">
              {todosHeadsState.map((td) => {
                return (
                  <Col key={td.id} id={td.id} lg={3}>
                    <div className="todosHead_head">
                      <h3 className="todosHead_head_title mb-0">{td.title}</h3>
                    </div>
                  </Col>
                );
              })}
              <Col lg={3}>
                <div className="todosHead_head">
                  <h3 className="todosHead_head_title mb-0">Options</h3>
                </div>
              </Col>
            </Row>
            <Row className="g-0">{todosList}</Row>
            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Todo</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <h3 className="h3">{todoData.todoTitle}</h3>

                <p>{todoData.todoDec}</p>
              </Modal.Body>
            </Modal>

            <Modal show={show2} onHide={handleClose2}>
              <Modal.Header closeButton>
                <Modal.Title>Add New Todo</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form>
                  {/* db.collection("todos").doc(todoData.id).update({
                    todoTitle: enteredTitleRef,
                    todoDec: enteredDecRef,
                  }); */}
                  <Form.Group className="mb-3" controlId="formTodoTitle">
                    <Form.Label>Todo Title</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Type your todo Title here"
                      name="formTodoTitle"
                      min="3"
                      required
                      value={todoData.todoTitle}
                      onChange={handleChange}
                      ref={formTodoTitleRef}
                    />
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formTodoDescription">
                    <Form.Label>Todo Description </Form.Label>
                    <Form.Control
                      as="textarea"
                      placeholder="Type your todo Description here"
                      rows={3}
                      name="formTodoDescription"
                      ref={formTodoDescriptionRef}
                      min="15"
                      required
                      value={todoData.todoDec}
                      onChange={handleChange}
                    />
                  </Form.Group>

                  <Button
                    variant="primary"
                    type="submit"
                    onClick={(e) => {
                      e.preventDefault();
                      db.collection("todos").doc(todoData.id).update({
                        todoTitle: todoData.todoTitle,
                        todoDec: todoData.todoDec,
                      });
                      handleClose2();
                    }}
                  >
                    Edit
                  </Button>
                </Form>
              </Modal.Body>
            </Modal>
          </Container>
        </div>
      </DragDropContext>
    </>
  );
}
