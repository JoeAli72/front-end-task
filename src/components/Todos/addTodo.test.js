import React from 'react'
import { render, screen, cleanup, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import AddTodo from './AddTodo';

test('should render AddTodo component', () => {
    render(<AddTodo />);
    const addButton = screen.getByTestId('test-1');
    expect(addButton).toBeInTheDocument();
    expect(addButton).toHaveTextContent('Add');
    expect(addButton).toBeTruthy(); 

    fireEvent.click(addButton)

    expect(addButton).toBeInTheDocument();

});