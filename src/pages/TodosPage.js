import TodosContent from '../components/Todos';

const TodosPage = () => {
  return <TodosContent />;
};

export default TodosPage;
