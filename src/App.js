import { useContext } from "react";

import { Switch, Route, Redirect } from "react-router-dom";

import Layout from "./components/Layout/Layout";
import TodosPage from "./pages/TodosPage";
import AuthPage from "./pages/AuthPage";
import HomePage from "./pages/HomePage";

import AuthContext from "./store/auth-context";

import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  const authCtx = useContext(AuthContext);

  return (
    <Layout>
      <Switch>
        <Route path="/" exact>
          <HomePage />
        </Route>
        {!authCtx.isLoggedIn && (
          <Route path="/auth">
            <AuthPage />
          </Route>
        )}

        <Route path="/todo-list">
          {authCtx.isLoggedIn && <TodosPage />}
          {!authCtx.isLoggedIn && <Redirect to="/auth" />}
        </Route>

        <Route path="*">
          <Redirect to="/" />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
